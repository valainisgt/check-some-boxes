const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
      index: path.resolve(__dirname, 'src', 'index.tsx'),
      viewer: path.resolve(__dirname, 'src', 'viewer.tsx')
  },
  plugins: [
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'src', 'index.html'),
        chunks: ['index']
      }),
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'src', 'index.html'),
        chunks: ['viewer'],
        filename: 'viewer.html'
      })
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      }
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    filename: 'bundle.[contenthash].js',
    path: path.resolve(__dirname, 'dist'),
    clean: true,
  },
  devtool: 'source-map'
};
