export interface Task {
    text: string
    isComplete: boolean
}

export interface State {
    tasks: [number,Task][],
    backgroundColor: string,
    textColor: string
}

export const nullState = (): State => ({
    tasks: [],
    backgroundColor: "#ff0000",
    textColor: "#0000ff"
})

const actionTypes = {
    TASK_ADDED: 'TASK_ADDED',
    TASK_UPDATED: 'TASK_UPDATED',
    TASK_REMOVED: 'TASK_REMOVED',
    VIEWER_LAUNCHED: 'VIEWER_LAUNCHED',
    BACKGROUNDCOLOR_UPDATED: 'BACKGROUNDCOLOR_UPDATED',
    TEXTCOLOR_UPDATED: 'TEXTCOLOR_UPDATED'
}

interface Action {
    type: string
    payload: any
}

interface Dispatch {
    (x: Action): void
}

export interface Behaviors {
    addTask: (x: string) => void
    updateTask: (id: number, task: Task) => void
    removeTask: (id: number) => void
    launchViewer: () => void
    updateBackgroundColor: (color: string) => void
    updateTextColor: (color: string) => void
}

interface Store<T> {
    add: (item: T) => void
    update: (id: number, item: T) => void
    items: () => [number,T][]
    remove: (id: number) => void
}

function externalIdentityStore<T>(): Store<T> {
    const idGenerator = (() => {
        let counter = 0
        return () => ++counter
    })()
    let items: [number,T][] = []
    return {
        add: (item: T): void => {
            const id = idGenerator()
            items = items.concat([[id,item]])
        },
        update: (id: number, item: T): void => {
            items = items.map(([x,y]) => {
                const newTask = x === id ? item : y;
                return [x, newTask]
            })
        },
        items: (): [number,T][] => items.concat([]),
        remove: (id: number) => {
            items =  items.filter(([x,_]) => x !== id)
        }
    }
}

export interface Reducer {
    (state: State, action: Action): State
}

import { toQueryString } from "./ViewerQueryParameters"
export const viewerWindowMiddleware = (): Reducer => {
    let viewerWindow : Window | null = null
    return (state: State, action: Action): State => {
        const url = `viewer?${toQueryString(state.tasks.map(([_,x]) => x), state.backgroundColor, state.textColor)}`
        if (action.type === actionTypes.VIEWER_LAUNCHED) {
            viewerWindow = window.open(url, "viewer", "width=300,height=250,menubar=no,status=no,titlebar=no,toolbar=no")
        }
        else {
            if(!!viewerWindow && !viewerWindow.closed) {
                viewerWindow.location = url
            }
        }
        return state
    }
}

export const reducer = (taskStore: Store<Task> = externalIdentityStore<Task>()): Reducer => (state: State, action: Action): State => {
    if (action.type === actionTypes.TASK_ADDED) {
        taskStore.add({ text: action.payload, isComplete: false })
        return {
            ...state,
            tasks: taskStore.items()
        }
    }
    else if (action.type === actionTypes.TASK_UPDATED) {
        taskStore.update(action.payload.id, action.payload.task)
        return {
            ...state,
            tasks: taskStore.items()
        }
    }
    else if (action.type === actionTypes.TASK_REMOVED) {
        taskStore.remove(action.payload)
        return {
            ...state,
            tasks: taskStore.items()
        }
    }
    else if (action.type === actionTypes.BACKGROUNDCOLOR_UPDATED) {
        return {
            ...state,
            backgroundColor: action.payload
        }
    }
    else if (action.type === actionTypes.TEXTCOLOR_UPDATED) {
        return {
            ...state,
            textColor: action.payload
        }
    }
    else {
        return state
    }
}

export const combineReducers = (reducers: Reducer[]): Reducer => (state: State, action: Action) => reducers.reduce((acc, x) => x(acc,action), state)

export const nullBehaviors = (): Behaviors => ({
    addTask: () => {},
    updateTask: () => {},
    removeTask: () => {},
    launchViewer: () => {},
    updateBackgroundColor: () => {},
    updateTextColor: () => {}
})

export const dispatchBehaviors = (dispatch: Dispatch): Behaviors => ({
    addTask: x => dispatch({ type: actionTypes.TASK_ADDED, payload: x }),
    updateTask: (id: number, task: Task) => dispatch({ type: actionTypes.TASK_UPDATED, payload: { id, task }}),
    removeTask: (id: number) => dispatch({ type: actionTypes.TASK_REMOVED, payload: id }),
    launchViewer: () => dispatch({ type: actionTypes.VIEWER_LAUNCHED, payload: undefined }),
    updateBackgroundColor: (x: string) => dispatch({ type: actionTypes.BACKGROUNDCOLOR_UPDATED, payload: x }),
    updateTextColor: (x: string) => dispatch({ type: actionTypes.TEXTCOLOR_UPDATED, payload: x })
})
