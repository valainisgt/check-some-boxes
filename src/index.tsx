import React, { createContext, useReducer, useContext, useState, useCallback } from "react"
import { render } from "react-dom"
import { Text, ThemeProvider, Stack, TextField, Checkbox, initializeIcons, IconButton, PrimaryButton, ColorPicker, VerticalDivider, IColor, rgb2hex } from "@fluentui/react"
import { nullState, nullBehaviors, reducer, combineReducers, viewerWindowMiddleware, State, dispatchBehaviors, Behaviors } from "./stateManagement"
import { debounce } from "lodash"

const initialState = nullState()

interface Context {
    state: State,
    behaviors: Behaviors
}

const storeContext = createContext<Context>({
    state: initialState,
    behaviors: nullBehaviors()
})

const appliedReducer = combineReducers([reducer(),viewerWindowMiddleware()])

const storeProvider = (initialState: State, Context: React.Context<Context>) => ({ children }: { children: any }) => {
    const [state, dispatch] = useReducer(appliedReducer, initialState)
    const behaviors = dispatchBehaviors(dispatch)
    return <Context.Provider value={{state, behaviors}}>{children}</Context.Provider>
}

const DefaultProvider = storeProvider(initialState, storeContext)

import { Task } from "./stateManagement"
const TaskComponent = (props: { task: Task; handleChange: (x: boolean) => void; handleClick: () => void }) => {
    return <Stack horizontal verticalAlign="center" horizontalAlign="space-between">
        <Checkbox label={props.task.text} checked={props.task.isComplete} onChange={(_,checked) => {
            props.handleChange(checked || false) }} />
        <IconButton iconProps={{ iconName: 'Cancel' }} onClick={props.handleClick} />
    </Stack>
}

const app = (context: React.Context<Context>) => () => {
    const {state, behaviors} = useContext(context)
    const [newTask, setNewTask] = useState("")
    const onChangeBackgroundColor = useCallback(debounce((e: any, color: IColor) => {
        behaviors.updateBackgroundColor(`#${rgb2hex(color.r, color.g, color.b)}`)
    }, 100), [behaviors])
    const onChangeTextColor = useCallback(debounce((e: any, color: IColor) => {
        behaviors.updateTextColor(`#${rgb2hex(color.r, color.g, color.b)}`)
    }, 100), [behaviors])
    return <div className="w3-container">
        <div className="w3-card w3-panel w3-padding-16">
            <Stack tokens={{childrenGap: 5 }}>
                <Stack.Item align="center">
                    <Text variant="xxLarge">Let's Check Some Boxes!</Text>
                </Stack.Item>
                <VerticalDivider />
                <Stack.Item align="center">
                    <Text variant="xLarge">Design Your List Overlay</Text>
                </Stack.Item>
                <Stack horizontal horizontalAlign="center" tokens={{ childrenGap: "5%" }}>
                    <Stack>
                        <Stack.Item align="center"><Text variant="large">Background Color</Text></Stack.Item>
                        <ColorPicker color={state.backgroundColor} alphaType="none" showPreview={true} onChange={onChangeBackgroundColor} />
                    </Stack>
                    <Stack>
                        <Stack.Item align="center"><Text variant="large">Text Color</Text></Stack.Item>
                        <ColorPicker color={state.textColor} alphaType="none" showPreview={true} onChange={onChangeTextColor} />
                    </Stack>
                </Stack>
                <Stack.Item align="center">
                    <Text variant="xLarge">Add Your List Items</Text>
                </Stack.Item>
                <Stack.Item>
                    <TextField placeholder="What needs to be done?" id="newTask" value={newTask} onChange={(_,value) => setNewTask(value || "")} onKeyPress={e => {
                        if(e.key === 'Enter') {
                            const t = e.target as HTMLInputElement
                            if(t.value) {
                                behaviors.addTask(t.value)
                                setNewTask("")
                            }
                        }
                    }} />
                </Stack.Item>
                <Stack id="tasks" role="grid">
                    {state.tasks.map(([id, task]) => (<TaskComponent task={task} handleChange={(isComplete: boolean) => {
                        behaviors.updateTask(id, { ...task, isComplete })
                    }} key={id} handleClick={() => { behaviors.removeTask(id) }} />))}
                </Stack>
                <Stack.Item align="center">
                    <PrimaryButton text="Launch Task List" onClick={() => {
                        behaviors.launchViewer()
                    }} />
                </Stack.Item>
            </Stack>
        </div>
    </div>
}

const App = app(storeContext)
initializeIcons()

render(<DefaultProvider><ThemeProvider><App /></ThemeProvider></DefaultProvider>, document.querySelector("main"))
