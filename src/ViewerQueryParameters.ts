import { parse, stringify } from "querystring"
import { Task } from "./stateManagement"

interface ViewerQueryParameters {
    tasks: string[]
    completed: number
    backgroundColor: string
    textColor: string
}

export const fromQueryString = (candidate: string): { tasks: Task[], backgroundColor: string, textColor: string } => {
    candidate = candidate.replace('?', '')
    const unsafeQueryParams = parse(candidate)
    const createQueryParams = ({ tasks = [], completed = 0, backgroundColor = "", textColor = "" } = {}): ViewerQueryParameters => {
        tasks = [].concat(tasks)
        const unsafeCompleted = parseInt(completed as any)
        completed = isNaN(unsafeCompleted) ? 0 : completed
        return { tasks, completed, backgroundColor, textColor }
    }
    const queryParams = createQueryParams(unsafeQueryParams)

    return {
        tasks: queryParams.tasks.map((x, index) => ({
            text: x,
            isComplete: (queryParams.completed & Math.pow(2,index)) === Math.pow(2,index)
        })),
        backgroundColor: queryParams.backgroundColor,
        textColor: queryParams.textColor
    }
}

export const toQueryString = (tasks: Task[], backgroundColor: string, textColor: string): string => {
    const queryParams: ViewerQueryParameters = {
        tasks: tasks.map(x => x.text),
        completed: tasks
            .map((task, index) => !!task.isComplete ? Math.pow(2,index) : 0)
            .reduce((acc,x) => acc | x, 0),
        backgroundColor,
        textColor
    }
    return stringify(queryParams as any)
}
