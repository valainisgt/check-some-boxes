import React from "react";
import { render } from "react-dom";
import { Text, ThemeProvider, Stack, initializeIcons, Icon, createTheme } from "@fluentui/react"
import { fromQueryString } from "./ViewerQueryParameters"

const state = fromQueryString(window.location.search)
const theme = createTheme({
    palette: {
    white: state.backgroundColor,
    neutralPrimary: state.textColor
}})
const App = () => {
    return <Stack>
        {state.tasks.map((x,index) => {
            return <Stack horizontal verticalAlign="center" horizontalAlign="space-between" key={`${x.text}${index}`}>
                <Text>{x.text}</Text>
                <Icon iconName={x.isComplete ? "CheckboxComposite" : "Checkbox" } />
            </Stack>
        })}
    </Stack>
}

initializeIcons()

render(<ThemeProvider theme={theme}><div className="w3-container w3-padding-small" style={{ height: "100vh"}}><App /></div></ThemeProvider>, document.querySelector("main"))
