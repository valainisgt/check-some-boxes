import { expect } from '@playwright/test';
import { test } from "./spaFixture"
import { pageObjectModel } from "./pageObjectModel"
import { getColorFromString } from "@fluentui/react"

test('Add Tasks', async ({ page, url }) => {
  await page.goto(url);
  const pageModel = pageObjectModel(page)

  let text = "Task 1"
  await pageModel.addTask(text)
  await expect(page.locator("#tasks")).toContainText(text)

  text = "Task 2"
  await pageModel.addTask(text)
  await expect(page.locator("#tasks")).toContainText(text)
})

test('Clears New Task after adding', async ({ page, url }) => {
    await page.goto(url)
    const pageModel = pageObjectModel(page)

    let text = "Task 1"
    await pageModel.addTask(text)
    await expect(page.locator("#newTask")).toBeEmpty()
})

test('Complete Task', async ({ page, url }) => {
    await page.goto(url)
    const pageModel = pageObjectModel(page)

    let text = "Task 1"
    await pageModel.addTask(text)

    await page.click(`text=${text}`)

    await expect(page.locator(`text=${text}`)).toBeChecked()
})

test('Remove Task', async ({ page, url }) => {
    await page.goto(url)
    const pageModel = pageObjectModel(page)

    let text = "Task 1"
    await pageModel.addTask(text)

    await page.click(`#tasks button:right-of(:text("${text}"))`)

    await expect(page.locator("#tasks")).not.toContainText(text)
})

test('Launch Task List', async ({ page, url }) => {
    await page.goto(url)
    const pageModel = pageObjectModel(page)

    const task1Text = "Task 1"
    await pageModel.addTask(task1Text)

    const task2Text = "Task 2"
    await pageModel.addTask(task2Text)

    await page.click(`text=${task1Text}`)

    const popup = await pageModel.launchTaskList()

    await expect(popup.locator(`text=\"${task1Text}\"`)).toBeVisible()
    await expect(popup.locator(`text=\"${task2Text}\"`)).toBeVisible()
    await expect(popup.locator(`[data-icon-name="CheckboxComposite"]:right-of(:text("${task1Text}"))`))
    await expect(popup.locator(`[data-icon-name="Checkbox"]:right-of(:text("${task2Text}"))`))
})

test('Task List Updates', async ({ page, url }) => {
    await page.goto(url)
    const pageModel = pageObjectModel(page)

    const popup = await pageModel.launchTaskList()

    const task1Text = "Task 1"
    await pageModel.addTask(task1Text)

    const task2Text = "Task 2"
    await pageModel.addTask(task2Text)

    await page.click(`text=${task1Text}`)

    await expect(popup.locator(`text=\"${task1Text}\"`)).toBeVisible()
    await expect(popup.locator(`text=\"${task2Text}\"`)).toBeVisible()
    await expect(popup.locator(`[data-icon-name="CheckboxComposite"]:right-of(:text("${task1Text}"))`))
    await expect(popup.locator(`[data-icon-name="Checkbox"]:right-of(:text("${task2Text}"))`))
})

test('Viewer Uses Selected Colors', async ({ page, url }) => {

    const toCssRgb = (color: string): string => {
        const colorObject = getColorFromString(`#${color}`)
        return `rgb(${colorObject?.r}, ${colorObject?.g}, ${colorObject?.b})`
    }
    await page.goto(url)
    const pageModel = pageObjectModel(page)

    const task1Text = "Task 1"
    await pageModel.addTask(task1Text)

    const backgroundColor = "aabbcc"
    await page.fill('[aria-label="Hex"] >> nth=0', backgroundColor)

    const textColor = "ccbbaa"
    await page.fill('[aria-label="Hex"] >> nth=1', textColor)

    const popup = await pageModel.launchTaskList()

    // Determining what the background color is has proven to be challenging.
    // This selector is very brittle, but should not change much
    await expect(popup.locator("main > div")).toHaveCSS("background-color", toCssRgb(backgroundColor))
    await expect(popup.locator(`text=\"${task1Text}\"`)).toHaveCSS("color", toCssRgb(textColor))
})
