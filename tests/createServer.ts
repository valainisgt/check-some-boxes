import express from "express"
import http from "http"
import { resolve, join } from "path"

export const createServer = () => {
    const app = express();
    app.use(express.static(resolve(__dirname, "..", "dist"), { extensions: ['html'] }));

    app.get("*", (_,res) => {
        res.sendFile(join(resolve(__dirname, "..", "dist"), "index.html"));
    });

    return http.createServer(app);
}
