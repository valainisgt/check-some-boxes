import { test as base } from "@playwright/test"
import { createServer } from "./createServer"

export const test = base.extend<{},{ url: string }>({
    url: [ async ({}, use, workerInfo) => {

        const port = 3000 + workerInfo.workerIndex;
        const url = `http://localhost:${port}`;

        const server = createServer()

        const startSignal = new Promise(r => server.on("listening", r));
        const stopSignal = new Promise(r => server.on("close", r));

        server.listen(port);
        await startSignal;

        await use(url);

        server.close();
        await stopSignal;

    }, { scope: 'worker', auto: true } ]
})
