import { Page, Locator } from "@playwright/test"

interface PageObject {
    addTask: (text: string) => Promise<void>
    launchTaskList: () => Promise<Page>
}

export const pageObjectModel = (page: Page): PageObject => {
    const newTask: Locator = page.locator('[placeholder="What needs to be done?"]')
    const launchTaskListButton: Locator = page.locator('text="Launch Task List"')
    return {
        addTask: async (text: string) => {
            await newTask.fill(text);
            await newTask.press("Enter");
        },
        launchTaskList: async () => {
            const [ popup ] = await Promise.all([
                page.waitForEvent('popup'),
                launchTaskListButton.click()
            ])
            await popup.waitForLoadState('load')
            return popup;
        }
    }
}
